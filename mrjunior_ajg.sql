# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.38)
# Database: mrjunior_ajg
# Generation Time: 2015-10-20 03:06:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT '',
  `password` text,
  `date_created` int(11) DEFAULT NULL,
  `role` int(1) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  `fullname` text,
  `belongsTo` int(11) DEFAULT NULL,
  `phone` text,
  `address` text,
  `ic_no` decimal(12,0) DEFAULT NULL,
  `mavro_id` int(11) DEFAULT NULL,
  `account_no` decimal(25,0) DEFAULT NULL,
  `bank` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ic_no` (`ic_no`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;

INSERT INTO `account` (`id`, `email`, `password`, `date_created`, `role`, `last_login`, `fullname`, `belongsTo`, `phone`, `address`, `ic_no`, `mavro_id`, `account_no`, `bank`)
VALUES
	(1,'admin','21232f297a57a5a743894a0e4a801fc3',1444744157,10,1445305416,'Administrator',0,'0123456789','Johor Bahru',888888888881,NULL,123,'CIMB'),
	(35,'ahmad','21232f297a57a5a743894a0e4a801fc3',1444744157,20,1445305458,'Ahmad',1,'0123456789','Johor Bahru',888888888882,1,12423,'MBB'),
	(36,'abu','21232f297a57a5a743894a0e4a801fc3',1444744157,20,1445227715,'Abu Tausi',35,'0123456789','Johor Bahru',888888888883,2,151016920863,'MBB'),
	(38,'zamani','21232f297a57a5a743894a0e4a801fc3',1444744157,20,1445054208,'Zamani Azman',35,'0123456789','Johor Bahru',888888888884,2,151016920863,'MBB'),
	(40,'fixed1','21232f297a57a5a743894a0e4a801fc3',1444744157,30,1445255040,'fixed1',1,'0123456789','Johor Bahru',1,NULL,123,'CIMB'),
	(41,'fixed2','21232f297a57a5a743894a0e4a801fc3',1444744157,30,1445224586,'fixed2',1,'0123456789','Johor Bahru',2,NULL,123,'CIMB'),
	(42,'fixed3','21232f297a57a5a743894a0e4a801fc3',1444744157,30,1445253417,'fixed3',1,'0123456789','Johor Bahru',3,NULL,123,'CIMB'),
	(44,'fixed4','21232f297a57a5a743894a0e4a801fc3',1444744157,30,1445224586,'fixed4',1,'0123456789','Johor Bahru',4,NULL,123,'CIMB'),
	(45,'fixed5','21232f297a57a5a743894a0e4a801fc3',1444744157,30,1445224586,'fixed5',1,'0123456789','Johor Bahru',5,NULL,123,'CIMB');

/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mavro
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mavro`;

CREATE TABLE `mavro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mavro` WRITE;
/*!40000 ALTER TABLE `mavro` DISABLE KEYS */;

INSERT INTO `mavro` (`id`, `pin`)
VALUES
	(1,'123'),
	(2,'345');

/*!40000 ALTER TABLE `mavro` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table request
# ------------------------------------------------------------

DROP TABLE IF EXISTS `request`;

CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `gh_amount` int(11) DEFAULT NULL,
  `ph_amount` int(11) DEFAULT NULL,
  `type` int(1) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `contributors` varchar(255) DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  `date_generated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` text NOT NULL,
  `access` int(11) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
