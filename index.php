<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AJG Community: Login</title>
    <link rel="stylesheet" type="text/css" href="static/css/ajg.css">
    <script src="static/js/min/libraries-min.js"></script>
</head>
<body id="login_index">
    <?php include 'static/templates/messages.php'; ?>
    <div class="container">
        <div class="login_container center-block">
            <form id="login_form" method="POST" action="app/login">
                <img class="logo" src="static/img/ajg_logo.png">
                <h3>Login</h3>
                <fieldset class="form-group">
                    <input class="form-control" name="email" placeholder="Email Address" required>
                </fieldset>
                <fieldset class="form-group">
                    <input class="form-control" name="password" placeholder="Password" type="password" required>
                </fieldset>
                <button type="submit" class="btn btn-primary btn-block">Login</button>
                <a class="small" href="#">Forgot Password?</a>
            </form>
        </div>
    </div>

</body>
</html>