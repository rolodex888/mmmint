<?php
// MESSAGES POPUP
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message']['text'];
    $type = $_SESSION['message']['type'];
?>

    <div class="alert ajg-alert alert-<?php echo $type; ?> alert-dismissable fade in">
        <div class="container">
            <div class="faux_container">
                <button type="button" class="close" data-dismiss="alert">
                    <span class="lnr lnr-cross"></span>
                </button>
                <?php if ($type == 'success') { ?><span class="lnr lnr-smile"></span>
                <?php } elseif ($type == 'danger') { ?><span class="lnr lnr-sad"></span>
                <?php } elseif ($type == 'warning') { ?><span class="lnr lnr-warning"></span>
                <?php } echo $message?>
            </div>
        </div>
    </div>

    <script>
        // Animate visibility of alerts.
        $('.alert').hide();
        $('.alert').slideDown('fast');

        // Pause timer on mouseenter
        var timer;

        timer = setTimeout(function() {
            $('.alert').alert('close');
        }, 8000);

        $('.alert').on('mouseenter', function() {
            clearTimeout(timer);
        }).on('mouseleave', function() {
            timer = setTimeout(function() {
                $('.alert').alert('close');
            }, 2000);
        })
    </script>

<?php
    unset($_SESSION['message']);
}

?>