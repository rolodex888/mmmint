<?php
include '../php/def.php';
if (!isset($activePage) && isset($_GET)) {
    $activePage = $_GET["page"];
}
?>

<nav id="ajg-nav" class="navbar navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="../static/img/logo_round.png"> AJG</a>
        <button class="text-primary navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#main_nav">
            &#9776;
        </button>
        <div class="collapse navbar-toggleable-sm" id="main_nav">
            <ul class="nav navbar-nav">
                <?php foreach($pages as $title=>$attr):?>
                    <li class="nav-item<?php if($activePage == $title): ?> active<?php endif; ?>">
                        <a class="nav-link" href="<?php echo $attr[0];?>">
                            <span class="lnr lnr-<?php echo $attr[1];?>"></span> <?php echo $title;?>
                        </a>
                    </li>
                <?php endforeach;?>
            </ul>
            <ul class="nav navbar-nav ajg-navbar-right">
                <li class="nav-item">
                    <a href="101.php?page=Profile" class="nav-link">
                        <span class="lnr lnr-user"></span> <?php echo $_SESSION['fullname']; ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="../php/router.php?command=logout" class="nav-link">
                        <span class="lnr lnr-exit"></span> Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
