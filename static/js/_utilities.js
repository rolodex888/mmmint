// Logs PHP vars into browsers console
$(function() {
    $('.countdown').each(function() {
        var a = $(this);
        var eventTime = $(this).attr('date'); // Timestamp - Sun, 21 Apr 2013 13:00:00 GMT
        var currentTime = Math.round(new Date().getTime()/1000); // Timestamp - Sun, 21 Apr 2013 12:30:00 GMT
        var diffTime = eventTime - currentTime;
        var duration = moment.duration(diffTime*1000, 'milliseconds');
        var interval = 1000;

        setInterval(function(){
            duration = moment.duration(duration - interval, 'milliseconds');
            a.text(duration.hours() + "h " + duration.minutes() + "m " + duration.seconds() + "s")
        }, interval);

        // if (duration < 0) {
        //     window.location.reload();
        // }
    })

})