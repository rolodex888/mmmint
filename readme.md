## AJG Data Model
* User
	* id _int_
	* email _varchar 255 unique_
	* password _text_
	* role _int_
	* category _int_
	* fullname _text_
	* group _int_
	* account _decimal 25_
	* bank _int_
	* phone _decimal 12_
	* ic_no _decimal 12 unique_
	* address _text_
	* belongsTo _int_
	* createdOn _int_
	* updatedOn _int_
	* accStatus _int_
	* mavroID _int_

* Requests
	* id _int_
	* createdBy _int references User.id_
	* amount _int_
	* type _int_
	* reqStatus
	* createdOn
	* updatedOn

--
### User Classes
* Account Status
	* Activated - 1
	* Pending - 2
	* Banned - 3
	* Deleted - 0

* Roles
	* Admin - 99
	* Pool - 1
	* Manager - 2
	* Participant - 3

* Categories
	* Platinum - 1
	* Gold - 2
	* Silver - 3
	* Bronze - 4

* Groups
	* 1 — ?
	* 2 — ?

* Banks
	* Maybank - 1
	* CIMB - 2
	* RHB - 3
	* Bank Islam - 4
	* Others - 5

### Request Classes
* Types
	* Provide Help - 1
	* Get Help - 2

* Request Status
	* Created
	* Pending
	* Verified
	* Completed