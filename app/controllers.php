<?php
include 'session.php';
include 'database.php';

if(!isset($_SESSION)){$session = new Session();}

class MYSQL {
    const auth_verify       =   'SELECT * FROM `user` WHERE email=:email AND password=:password';
    const auth_last_login   =   'UPDATE user SET lastLogin=:time WHERE id=:userid';

    // REQUESTS
    const checkReqStat      =   'SELECT * FROM request WHERE createdBy=:user_id AND reqStatus <> :completed ORDER BY createdOn ASC';
    const newRequest        =   'INSERT INTO request (createdBy, amount, type, reqStatus, createdOn) VALUES (:userid, :amount, :type, :status, :createdOn)';
    const findSlot          =   'SELECT * FROM request WHERE type <> :type AND reqStatus <> :completed AND reqStatus <> :cancelled AND reqStatus <> :verified AND createdBy <> :uid ORDER BY createdOn ASC';
    const partCount         =   'SELECT * FROM request WHERE createdBy=:createdBy AND reqStatus <> :completed AND type <> :type';
    const addContributor    =   'INSERT INTO request (createdBy, amount, type, participants, reqStatus, createdOn, updatedOn) VALUES (:createdBy, :amount, :type, :participants, :status, :created, :updated)';
    const updateContributor =   'UPDATE request SET participants=:self_id, reqStatus=:pending, updatedOn=:time WHERE id=:request_id AND reqStatus <> :completed';

    const updateSelf        =   'UPDATE request SET participants=:request_id, reqStatus=:pending, updatedOn=:time WHERE id=:self_id AND reqStatus <> :completed';

    const getlist           =   'SELECT ac.fullname, ac.email, ac.phone, ac.bank, ac.account_no, req.* FROM request req
                                    JOIN request contr ON contr.id=req.participants
                                    JOIN `user` ac ON contr.createdBy=ac.id
                                 WHERE req.createdBy=:createdBy AND req.reqStatus <> 99';

    const approve           =   'UPDATE request SET updatedOn=:time, reqStatus=3 WHERE id=:id'; //  AND reqStatus=2 — This is removed to help people approve even if it is more than 48hrs.
    const setComplete       =   'UPDATE request SET reqStatus=:completed, updatedOn=:time WHERE createdBy=:userid AND reqStatus=3';
    const setFailed         =   'UPDATE request SET reqStatus=:failed, updatedOn=:time WHERE createdBy=:participants AND participants=:createdBy AND reqStatus=:pending';

    // PARTICIPANTS
    const listparticipants  =   'SELECT u.fullname, u.phone, u.email, ref.fullname as belongsTo, u.lastLogin FROM user u JOIN user ref ON u.belongsTo=ref.id WHERE u.id <> :id';

    // DEBUGGIN CONSTANTS
    const convertType       =   'UPDATE request SET type=:type WHERE createdBy=:userid AND reqStatus=:created';
    const resetRequest      =   'TRUNCATE TABLE request';
    const resetAutoIncr     =   'ALTER TABLE request AUTO_INCREMENT=200';


}

function getuserid() {
    $raw = base64_decode($_SESSION['userid']);
    $uid = explode(DEF::KEY, $raw);
    return $uid[1];
}

class Participant {
    public function getParticipants($search=false) {
        if (!$search) {
            $query = MYSQL::listparticipants;
        } else {
            // Query for search here.
        }
        $nonAdmin = '';
        if ($_SESSION['role'] != DEF::ROLE_ADMIN) {
            $nonAdmin = ' AND u.belongsTo=:id';
        }

        $response['list'] = dbexec($query.$nonAdmin, [':id'=> getuserid()], 'fetchset');
        return $response;
    }

    public function addParticipant(){
        return true;
    }
}

class Request {
    // Internal Functions
    // =======================================================
    private function decryptid($data) {
        $a          = base64_decode($data);
        $b          = explode(DEF::KEY, $a);
        $decrypted  = $b[1];
        return $decrypted;
    }

    private function occur($arr) {
        $a = [];
        foreach ($arr as $k => $v) {
            array_push($a, $v->createdBy);
        }
        return array_count_values($a);
    }

    // Checks status
    // =======================================================
    public function init() {
        // Check table request for createdBy and status != complete
        $response = [
            'list'              => null,
            'status'            => 0,
            'type'              => DEF::TYPE_PH,
            'completed'         => false,
            'first_time'        => false,
            'balance'           => 0,
            'complete_failure'  => false
        ];

        $param = [
            ':user_id'      => $this->decryptid($_SESSION['userid']),
            ':completed'    => DEF::REQ_COMPLETED
        ];

        $exist = dbexec(MYSQL::checkReqStat, $param);

        // If exist, user is not a first time user, so response will be defaulted.
        if ($exist) {
            $response['status'] = $exist->reqStatus;
            $response['type']   = $exist->type;
            $response['amount'] = $exist->amount;

            if ($exist->reqStatus == DEF::REQ_CREATED) {
                $l = $this->generateList($exist);
            } else {
                $l = $this->getList($exist);
                $response['completed'] = $l['completed'];
            }
            $response['list'] = $l['list'];

            $failed = [];
            if ($l['list'] != 'wait') {
                // Count balance excluding failed.
                foreach ($l['list'] as $k => $v) {
                    if ($v->reqStatus == DEF::REQ_FAILED) {
                        array_push($failed, $v->reqStatus);
                    }
                }

                // Count failures
                $failures = array_count_values($failed);
                if(isset($failures[DEF::REQ_FAILED])) {
                    if ($failures[DEF::REQ_FAILED] == sizeof($l['list'])) {
                        if ($exist->type == DEF::TYPE_PH) {
                            $response['complete_failure'] = true;
                        } else {
                            $response['list'] = 'wait';
                        }
                    }
                }

                $response['balance'] = abs((sizeof($l['list']) - sizeof($failed)) * DEF::PH_MULTIPLIER - $exist->amount);
            }
        } else {
            // First time user only.
            if ($_SESSION['role'] != DEF::ROLE_ADMIN) {
                // Check if the user has exceeded the 14 days time
                if ($_SESSION['createdOn'] + DEF::FIRST_TIME_INT >= time()) {
                    $response['first_time'] = true;
                    $response['timeleft']   = DEF::FIRST_TIME_INT + $_SESSION['createdOn'];
                }
            }
        }

        return $response;
    }

    // Creates a new request. Returns boolean.
    // =======================================================
    public function newrequest($type, $amount) {
        $param = [
            ':userid'   => $this->decryptid($_SESSION['userid']),
            ':amount'   => $amount,
            ':type'     => $type,
            ':status'   => 1,
            ':createdOn'=> time()
        ];
        $response = dbexec(MYSQL::newRequest, $param, 'insert');
        return $response;
    }

    // Generates ph or gh list. This is the most challenging!
    // =======================================================
    public function generatelist($data) {
        $list = [];
        $param = [
            ':type'         => $data->type,
            ':completed'    => DEF::REQ_COMPLETED,
            ':cancelled'    => DEF::REQ_CANCELLED,
            ':verified'     => DEF::REQ_VERIFIED,
            ':uid'          => $this->decryptid($_SESSION['userid'])
        ];
        $slots = dbexec(MYSQL::findSlot, $param, 'fetchset');
        $self_is_null = ($data->participants) ? false:true;

        $amount = $data->amount;
        $c = $this->occur($slots);
        foreach ($c as $ck => $cv) {
            $r_param = [
                ':createdBy' => $ck,
                ':completed' => DEF::REQ_COMPLETED,
                ':type'      => $data->type
            ];

            $r = dbexec(MYSQL::partCount, $r_param, 'fetchset');
            $enough = true;
            $add    = false;

            foreach ($r as $k => $v) {
                $_amount    = $v->amount;
                $_type      = $v->type;
                $_createdBy = $v->createdBy;
                $_rid       = $v->id;
                // Check for NULL participants.
                if ($v->participants) {
                    // Check if number of participants is enough (row count * MULTIPLIER).
                    $failed = ($v->reqStatus == DEF::REQ_FAILED) ? 1:0;
                    if (sizeof($r) * DEF::PH_MULTIPLIER - $failed < $v->amount) {
                        $enough = false;
                        $add    = true;
                        break;
                    }
                } else {
                    // Not sure whether to check the multiplier or not...hmmm
                    $enough     = false;
                    $add        = false;
                    break;
                }
            }
            // echo json_encode($_rid);

            // Finally we add to the row
            if ($amount > 0){
                if (!$enough) {
                    $param_self = [
                        ':request_id'   => $_rid,
                        ':pending'      => DEF::REQ_PENDING,
                        ':time'         => time(),
                        ':self_id'      => $data->id,
                        ':completed'    => DEF::REQ_COMPLETED
                    ];
                    // Update diri sendiri dulu.
                    $self_insert_id = false;
                    if (!$self_is_null) {
                        $param_addSelf = [
                            ':createdBy'    => $data->createdBy,
                            ':amount'       => $data->amount,
                            ':type'         => $data->type,
                            ':participants' => $_rid,
                            ':status'       => DEF::REQ_PENDING,
                            ':created'      => time(),
                            ':updated'      => time()
                        ];
                        $self_insert_id = dbexec(MYSQL::addContributor, $param_addSelf, 'insert');
                    } else {
                        dbexec(MYSQL::updateSelf, $param_self, 'update');
                        $self_is_null = false;
                    }
                    // Lepas tu baru add orang lain.
                    if ($add) { // Means add another row
                        $param_add = [
                            ':createdBy'    => $ck,
                            ':amount'       => $_amount,
                            ':type'         => $_type,
                            ':participants' => (!$self_insert_id) ? $data->id:$self_insert_id,
                            ':status'       => DEF::REQ_PENDING,
                            ':created'      => time(),
                            ':updated'      => time()
                        ];
                        dbexec(MYSQL::addContributor, $param_add, 'insert');
                    } else { // Update existing row to prevent NULL participants
                        dbexec(MYSQL::updateContributor, $param_self, 'update');
                        $add = true;
                    }
                    // Subtract the multiplier.
                    $amount -= DEF::PH_MULTIPLIER;
                }
            }
        }
        $list = $this->getList($data);
        return $list;
    }

    // Get list based on type. Returns array
    // =======================================================
    public function getList($data) {
        $list                   = dbexec(MYSQL::getlist, [':createdBy'=> $data->createdBy], 'fetchset');
        $response['completed']  = false;
        if (sizeof($list) == 0) {
            $response['list'] = 'wait';
        } else {
            $approvals = [];
            foreach ($list as $k => $v) {
                if ($data->type == DEF::TYPE_PH) {
                    // 24 hours from date updated
                    $v->deadline = $v->updatedOn + DEF::TRANSFER_TIME;
                    // Check 24 Hours status
                    if ($v->reqStatus == DEF::REQ_PENDING && $v->deadline < time()) {
                        // Set the status as failed and get a new list.
                        $param = [
                            ':createdBy'    => $v->participants,
                            ':participants' => $v->createdBy,
                            ':failed'       => DEF::REQ_FAILED,
                            ':pending'      => DEF::REQ_PENDING,
                            ':time'         => time(),
                        ];
                        dbexec(MYSQL::setFailed, $param, 'update');
                    }
                }
                $v->self_id  = base64_encode(DEF::SECRET.DEF::KEY.$v->id);
                $v->part_id  = base64_encode(DEF::SECRET.DEF::KEY.$v->participants);

                // Find approval status and pool it!
                if ($v->reqStatus == DEF::REQ_VERIFIED) {
                    array_push($approvals, $v->reqStatus);
                }
            }

            // Get approval count
            $ac = array_count_values($approvals);
            if (isset($ac[DEF::REQ_VERIFIED])) {
                if ($ac[DEF::REQ_VERIFIED] * DEF::PH_MULTIPLIER == $data->amount) {
                    // Check if user's PH Amount is fulfilled or not. (if RM2000 must have 5 PH)
                    $response['completed'] = base64_encode(DEF::SECRET.DEF::KEY.$data->id);
                }
            }
            $response['list'] = $list;
        }

        return $response;
    }
    // Restart Cycle.
    // =======================================================
    // 1 - Set all to 99
    // 2 - Create new request
    public function complete($data) {
        $param = [
            ':completed'    => DEF::REQ_COMPLETED,
            // ':id'           => $this->decryptid($data->data),
            ':userid'       => getuserid(),
            ':time'         => time()
        ];
        $c = dbexec(MYSQL::setComplete, $param, 'update');

        if ($c) {
            $type = ($data->type == DEF::TYPE_PH) ? DEF::TYPE_GH : DEF::TYPE_PH;
            $this->newrequest($type, $data->amount);
        }

        return $c;
    }


    // Approve payments. Returns boolean.
    // =======================================================
    public function approve($data) {
        foreach ($data as $k => $v) {
            $param = [
                ':time' => time(),
                ':id'   => $this->decryptid($v)
            ];
            // echo json_encode($param);
            $a = dbexec(MYSQL::approve, $param, 'update');
        }
        // exit;
        return $a;
    }



    // ***********************************************************
    // ***********************************************************

    // Debug Convert Type
    // =======================================================
    public function convert($type) {
        $param = [
            ':type'     => ($type == DEF::TYPE_GH) ? DEF::TYPE_PH : DEF::TYPE_GH,
            ':created'  => DEF::REQ_CREATED,
            ':userid'   => getuserid()
        ];
        $a = dbexec(MYSQL::convertType, $param, 'update');
        return $a;
    }

    public function clearRequest() {
        dbexec(MYSQL::resetRequest,[], 'update');
        dbexec(MYSQL::resetAutoIncr,[], 'update');
        return true;
    }
}

class Auth {
    // Login — Returns boolean
    public function login($email, $password) {
        $param = [
            ':email'    => $email,
            ':password' => md5($password)
        ];

        $obj = dbexec(MYSQL::auth_verify, $param);

        if ($obj) { // Login Success
            $p = [
                ':time'     => time(),
                ':userid'   => $obj->id
            ];

            global $session;

            $session->regenid();

            $_SESSION['userid']     = base64_encode(DEF::SECRET.DEF::KEY.$obj->id);
            $_SESSION['role']       = $obj->role;
            $_SESSION['fullname']   = $obj->fullname;
            $_SESSION['email']      = $obj->email;
            $_SESSION['accStatus']  = $obj->accStatus;
            $_SESSION['createdOn']  = $obj->createdOn;
            $_SESSION['updatedOn']  = $obj->updatedOn;

            dbexec(MYSQL::auth_last_login, $p, 'update');
        }

        return $obj;
    }
}