<?php
// Database Connection
if(!defined('DB_HOST')) { define('DB_HOST', 'localhost'); }
if(!defined('DB_NAME')) { define('DB_NAME', 'ampangjg'); }
if(!defined('DB_USER')) { define('DB_USER', 'root'); }
if(!defined('DB_PASS')) { define('DB_PASS', 'root'); }

class DEF {
    const PH_MULTIPLIER     = 400;
    const PH_MAX_AMOUNT     = 2000;
    const PH_POOL_AMOUNT    = 70;

    const TYPE_PH           = 11;
    const TYPE_GH           = 12;

    const REQ_COMPLETED     = 99;
    const REQ_CREATED       = 1;
    const REQ_PENDING       = 2;
    const REQ_VERIFIED      = 3;
    const REQ_FAILED        = 4;
    const REQ_CANCELLED     = 5;

    const USR_ACTIVE        = 1;
    const USR_PENDING       = 2;
    const USR_BANNED        = 3;
    const USR_DELETED       = 0;

    const ROLE_ADMIN        = 99;
    const ROLE_MANAGER      = 1;
    const ROLE_MEMBER       = 2;
    const ROLE_POOL         = 3;

    const CAT_PLATINUM      = 1;
    const CAT_GOLD          = 2;
    const CAT_SILVER        = 3;
    const CAT_BRONZE        = 4;

    const FIRST_TIME_INT    = 120; //1209600;  // 60s * 60m * 24h * 14 = 14 days
    const TRANSFER_TIME     = 30; //172800;   // 60s * 60m * 24h * 2 = 2 days
    const GH_INT            = 120; //432000;   // 60s * 60m * 24h * 5 = 5 days

    const SECRET            = 'Allah_is_watching_you_in_whatever_you_do-do_you_know_that?';
    const KEY               = '1956&-197';
}