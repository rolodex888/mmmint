<?php
class Session{
    private $db;
    protected  static $_instance;

    public static function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __construct(){
        // Instantiate new Database object
        $this->db = new db('alt');

        // Set handler to overide SESSION
        session_set_save_handler(
            array($this, "_open"),
            array($this, "_close"),
            array($this, "_read"),
            array($this, "_write"),
            array($this, "_destroy"),
            array($this, "_gc")
        );

        // Start the session
        @session_start();
        //@session_regenerate_id(true);
    }

    public function regenid(){
        session_regenerate_id(true);
    }

    public function _open(){
        if($this->db){
            return true;
        }
        return false;
    }

    public function _close(){
        if($this->db->close()){
            return true;
        }
        return false;
    }

    public function _read($id){
        $this->db->query('SELECT `data` FROM sessions WHERE id=:id');
        $prm = [':id' => $id];

        // Attempt execution
        if($this->db->execute($prm)){
            $row = $this->db->fetch('asc');
            return $row['data'];
        }else{
            return '';
        }
    }

    public function _write($id, $data){
        $access = time(); 
        $this->db->query('REPLACE INTO sessions VALUES (:id, :access, :data)');
        $prm = [
            ':id' => $id,
            ':access' => $access,
            ':data' => $data
        ];
        // Attempt Execution
        if($this->db->execute($prm)){
            return true;
        }
        return false;
    }

    public function _destroy($id){
        $this->db->query('DELETE FROM sessions WHERE id=:id'); 
        $prm = [':id' => $id];

        // Attempt execution
        if($this->db->execute($prm)){
            return true;
        }
        return false;
    }

    public function _gc($max){
        // Calculate what is to be deemed old
        $old = time() - $max;
        $this->db->query('DELETE FROM sessions WHERE access < :old');
        $prm = [':old' => $old];

        // Attempt execution
        if($this->db->execute($prm)){
            return true;
        }
        return false;
    }
}

?>
