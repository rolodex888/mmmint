<?php

include 'settings.php';
require 'Horus.php';
require 'controllers.php';

$app = new Horus();

// *******************************************************************
// TEMPLATE ENGINE
// *******************************************************************
require_once 'Twig/Autoloader.php';
Twig_Autoloader::register();

function _template($path, $data=array()) {
    $loader     = new Twig_Loader_Filesystem('../tpl');
    $twig       = new Twig_Environment($loader, array('debug'=>true));
    $twig->addExtension(new Twig_Extension_Debug());
    $template   = $twig->loadTemplate($path.'.twig');

    $twig->addGlobal('session', $_SESSION);

    echo $template->render($data);
}

// *******************************************************************
// AUTH ROUTES
// *******************************************************************

// LOGIN
// -------------
$app->on('POST /login', function() {
    $auth = new Auth();
    $email = $this->body->email;
    $password = $this->body->password;

    $login = $auth->login($email, $password);

    if ($login) {
        $this->redirect('../app');
    } else {
        $this->redirect('logout');
    }
});

// LOGOUT
// -------------
$app->on('GET /logout', function() {
    session_destroy();
    $this->redirect('../');
});

// *******************************************************************
// APPLICATION ROUTES
// *******************************************************************

// TODO: Middleware for session validation.

// DASHBOARD
// -------------
$app->on('GET /', function(){
    if ($_SESSION['userid']) {
        $req = new Request();
        $data = $req->init();
        $this->render(_template('dashboard', $data));
    } else {
        $this->redirect('/');
    }
});

$app->on('POST /', function(){
    if ($_SESSION['userid']) {
        $req = new Request();
        $type = $this->body->type;
        $amount = $this->body->amount;
        $a = $req->newrequest($type, $amount);
        if ($a) {
            $this->redirect('../app');
        }
    } else {
        $this->redirect('/');
    }
});

$app->on('POST /approve', function(){
    if ($_SESSION['userid']) {
        $req = new Request();
        $a = $req->approve([$this->body->data1,$this->body->data2]);
        if ($a) {
            $this->redirect('../app');
        }
    } else {
        $this->redirect('/');
    }
});

$app->on('POST /complete', function(){
    if ($_SESSION['userid']) {
        $req = new Request();
        $a = $req->complete($this->body);
        if ($a) {
            $this->redirect('../app');
        }
    } else {
        $this->redirect('/');
    }
});

// PARTICIPANTS
// -------------
$app->on('GET /participants', function(){
    if ($_SESSION['userid']) {
        if ($_SESSION['role'] == DEF::ROLE_ADMIN || $_SESSION['role'] == DEF::ROLE_MANAGER) {
            $req = new Participant();
            $data = $req->getParticipants();
            $this->render(_template('participants', $data));
        } else {
            $this->redirect('/app');
        }
    } else {
        $this->redirect('/');
    }
});

$app->on('POST /participants', function(){
    if ($_SESSION['userid']) {
        if ($_SESSION['role'] == DEF::ROLE_ADMIN || $_SESSION['role'] == DEF::ROLE_MANAGER) {
            $req = new Participant();
            $a = $req->addParticipant();
            if ($a) {
                $this->redirect('participants');
            }
        } else {
            $this->redirect('/app');
        }
    } else {
        $this->redirect('/');
    }
});

// MAVRO
// -------------
$app->on('GET /mavro', function(){
    if ($_SESSION['userid']) {
        $req = new Request();
        $data = $req->init();
        $this->render(_template('mavro', $data));
    } else {
        $this->redirect('/');
    }
});

// PROFILE
// -------------
$app->on('GET /profile', function(){
    if ($_SESSION['userid']) {
        $this->render(_template('profile'));
    } else {
        $this->redirect('/');
    }
});

// DEBUGGING
// -------------
$app->on('/debug/convert', function(){
    if ($_SESSION['userid']) {
        if ($_SESSION['role'] == DEF::ROLE_ADMIN) {
            $req = new Request();
            if(isset($this->query->type)) {
                $d = $req->convert($this->query->type);
            } elseif(isset($this->query->reset)) {
                $d = $req->clearRequest();
            }
        }
        $this->redirect('../');
    } else {
        $this->redirect('/');
    }
});
