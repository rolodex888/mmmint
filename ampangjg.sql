# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.38)
# Database: ampangjg
# Generation Time: 2015-10-23 12:41:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table request
# ------------------------------------------------------------

DROP TABLE IF EXISTS `request`;

CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdBy` int(11) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '1',
  `participants` int(11) DEFAULT NULL,
  `reqStatus` int(2) NOT NULL DEFAULT '1',
  `createdOn` int(11) DEFAULT NULL,
  `updatedOn` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `createdBy` (`createdBy`),
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` text NOT NULL,
  `access` int(11) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT '',
  `password` text NOT NULL,
  `role` int(2) NOT NULL DEFAULT '3',
  `category` int(2) NOT NULL DEFAULT '2',
  `group` int(2) NOT NULL DEFAULT '2',
  `fullname` text,
  `account_no` decimal(25,0) DEFAULT NULL,
  `bank` text,
  `phone` decimal(12,0) DEFAULT NULL,
  `ic_no` decimal(12,0) DEFAULT NULL,
  `address` text,
  `belongsTo` int(11) DEFAULT '0',
  `createadOn` int(11) DEFAULT NULL,
  `updatedOn` int(11) DEFAULT NULL,
  `accStatus` int(2) DEFAULT '2',
  `mavro_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ic_no` (`ic_no`),
  UNIQUE KEY `email` (`email`),
  KEY `belongsTo` (`belongsTo`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`belongsTo`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `password`, `role`, `category`, `group`, `fullname`, `account_no`, `bank`, `phone`, `ic_no`, `address`, `belongsTo`, `createadOn`, `updatedOn`, `accStatus`, `mavro_id`)
VALUES
	(1,'admin@mmmint.me','21232f297a57a5a743894a0e4a801fc3',99,1,1,'Rizan Bin Sudin',164294263572,'Maybank',199483030,NULL,NULL,1,NULL,NULL,1,NULL),
	(2,'nanasue@mmmint.me','21232f297a57a5a743894a0e4a801fc3',99,1,1,'Suzana binti Muhiyuddin',156084847015,'Maybank',176894588,NULL,NULL,2,NULL,NULL,1,NULL),
	(3,'ahmad@ahmad.com','21232f297a57a5a743894a0e4a801fc3',3,3,3,'Ahmad Suhaib Razali',156084847015,'Maybank',176894588,NULL,NULL,2,NULL,NULL,1,NULL),
	(4,'abu@abu.com','21232f297a57a5a743894a0e4a801fc3',3,3,3,'Abu Juraimi Kassim',156084847015,'Maybank',176894588,NULL,NULL,3,NULL,NULL,1,NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
